FROM fedora:36

RUN dnf install -y \
    clang \
    curl \
    git \
    ncurses-compat-libs \
    openssl1.1 \
    openssl-devel \
    perl \
    python3 \
    python3-pip \
    SDL2-devel \
    systemd-devel \
    unzip \
    wget \
    xz \
    && dnf clean all

ENV USER tadkeep
RUN groupadd "${USER}" --gid 1000
RUN useradd "${USER}" --uid 1000 --gid 1000 --groups "wheel" --shell "/bin/bash" --create-home --no-log-init
RUN echo "%wheel   ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers
USER "${USER}"
ENV HOME="/home/${USER}"
ENV PATH="${HOME}/.cargo/bin:${PATH}"
WORKDIR "${HOME}"

ARG RUST_RELEASE

RUN bash -c "$(curl https://sh.rustup.rs -sSf)" '' --default-toolchain "${RUST_RELEASE%.*}" -y

RUN rustup component add rustfmt clippy

RUN cargo install cargo-cache \
    && cargo cache -a

RUN cargo install espup ldproxy \
    && cargo cache -a

RUN espup install --export-file "${HOME}/.rust-esp32" --targets "esp32" --toolchain-version "${RUST_RELEASE}" \
    && cargo cache -a

RUN cargo install cargo-udeps \
    && cargo cache -a
